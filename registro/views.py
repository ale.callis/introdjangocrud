from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona
# Create your views here.

def index(request):
    return render(request,'index.html',{'nombre':"Alejandro",'elementos':Persona.objects.all()})

def registro(request):
    return render(request,'formulario.html',{})

def crear(request):
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    edad = request.POST.get('edad',0)
    persona = Persona(nombre=nombre,apellido=apellido,edad=edad)
    persona.save()
    return HttpResponse("nombre : "+nombre+" apellido: "+apellido)
def buscar(request,id):
    persona = Persona.objects.get(pk=id)
    return HttpResponse("nombre : "+persona.nombre+" apellido: "+persona.apellido)

def editar(request,id):
    persona = Persona.objects.get(pk=id)
    return render(request,'editar.html',{'persona':persona})

def editado(request,id):
    persona = Persona.objects.get(pk=id)
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    edad = request.POST.get('edad',0)
    persona.nombre = nombre
    persona.apellido = apellido
    persona.edad = edad
    persona.save()
    return HttpResponse("nombre : "+persona.nombre+" apellido: "+persona.apellido)

def eliminar(request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return HttpResponse("Persona eliminada")
    