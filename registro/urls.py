from django.urls import path
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    path('registro/',views.registro, name="registro"),
    path('registro/crear',views.crear, name="crear"),
    path('registro/buscar/<int:id>',views.buscar, name="buscar"),
    path('registro/editar/<int:id>',views.editar,name="editar"),
    path('registro/editado/<int:id>',views.editado,name="editado"),
    path('registro/eliminar/<int:id>',views.eliminar,name="eliminar")
]